package com.example.agavelab.ui.beers;


import android.content.Context;
import com.example.agavelab.data.Beer;

import java.util.ArrayList;

public interface BeersInterface {

    interface Presenter {
        void setView(BeersInterface.View view);

        void onDestroy();

        void requestBeers(Context context, int page, int per_page);

    }

    interface View {

        void onResponseFailure(Throwable throwable);

        void onResponseFailureNext(Throwable throwable);

        void showFirstBeers(ArrayList<Beer> beers);

        void showNextBeers(ArrayList<Beer> beers);

        void showErrorView(Throwable throwable);

        void hideErrorView();

        String fetchErrorMessage(Throwable throwable);

    }

    interface Interactor {
        interface OnFinishedListener {
            void onFinished(ArrayList<Beer> beers);

            void onFailure(Throwable t);
        }

        void getBeers(Context context, int page, int per_page, OnFinishedListener onFinishedListener);

    }

}
