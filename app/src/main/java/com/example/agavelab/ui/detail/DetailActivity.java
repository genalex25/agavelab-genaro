package com.example.agavelab.ui.detail;

import androidx.appcompat.app.AppCompatActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.agavelab.R;
import com.example.agavelab.data.Beer;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DetailActivity extends AppCompatActivity {

    Beer beer;
    @BindView(R.id.icon)
    CircleImageView icon;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtTagline)
    TextView txtTagline;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.txtBrewed)
    TextView txtBrewed;
    @BindView(R.id.txtFood)
    TextView txtFood;
    @BindView(R.id.txtAbv)
    TextView txtAbv;
    @BindView(R.id.txtIbu)
    TextView txtIbu;
    @BindView(R.id.viewSRM)
    View viewSRM;
    String[] colors = {"#FFE699","#FFD878","#FFCA5A","#FFBF42","#FBB123","#F8A600","#F39C00","#EA8F00","#E58500","#DE7C00","#D77200","#CF6900","#CB6200","#C35900","#BB5100",
            "#B54C00","#B04500","#A63E00","#A13700","#9B3200","#952D00","#8E2900","#882300","#821E00","#7B1A00","#771900","#701400","#6A0E00","#660D00","#5E0B00","#5A0A02",
            "#600903","#520907","#4C0505","#470606","#440607","#3F0708","#3B0607","#3A070B","#36080A"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        beer = (Beer)getIntent().getSerializableExtra("beer");
        if(beer!=null){
            fillBeer();
        }
    }

    private void fillBeer(){
        Glide.with(DetailActivity.this)
                .load(beer.getImage_url())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_beer)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(icon);

        txtName.setText(beer.getName());
        txtTagline.setText(beer.getTagline());
        txtDescription.setText(beer.getDescription());
        String bre = "First brewed Date : "+beer.getFirst_brewed();
        txtBrewed.setText(bre);
        String abv = "ABV : "+ beer.getAbv();
        txtAbv.setText(abv);
        String ibu = "IBU : "+ beer.getIbu();
        txtIbu.setText(ibu);
        int srm = Math.round(beer.getSrm());
        if(srm<40){
            viewSRM.setBackgroundColor(Color.parseColor(colors[srm]));
        }else{
            viewSRM.setBackgroundColor(Color.parseColor(colors[39]));
        }

        StringBuilder sb = new StringBuilder();
        for(String food : beer.getFood_pairing()){
            sb.append(food);
            sb.append(" ");
        }
        String food = "Food pairing : " + sb.toString();
        txtFood.setText(food);

    }
}
