package com.example.agavelab.ui.beers;

import android.content.Context;
import com.example.agavelab.R;
import com.example.agavelab.data.Beer;
import com.example.agavelab.network.Service;
import com.example.agavelab.network.Service_routes;
import com.orhanobut.logger.Logger;
import java.util.ArrayList;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class BeersInteractor implements BeersInterface.Interactor{

    @Override
    public void getBeers(Context context,int page, int per_page, OnFinishedListener onFinishedListener) {

        Service_routes client = Service.createService(Service_routes.class);
        client.getBeers(page,per_page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<Response<ArrayList<Beer>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ArrayList<Beer>> arrayListResponse) {
                        Logger.d("Is from cache communities : " + arrayListResponse.raw().cacheResponse());
                        Logger.d("Is from network communities: " + arrayListResponse.raw().networkResponse());

                        int statusCode = arrayListResponse.code();
                        Logger.d("Code : " + statusCode);
                        if (statusCode == 200) {
                            Logger.d("aki : " + statusCode);

                            ArrayList<Beer> pers = arrayListResponse.body();
                            if (pers != null) {
                                onFinishedListener.onFinished(pers);
                            }

                        } else {

                            onFinishedListener.onFailure(new Throwable(context.getResources().getString(R.string.error_msg_unknown)));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        onFinishedListener.onFailure(new Throwable(context.getResources().getString(R.string.error_msg_unknown)));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
