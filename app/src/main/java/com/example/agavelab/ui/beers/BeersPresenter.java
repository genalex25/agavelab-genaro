package com.example.agavelab.ui.beers;

import android.content.Context;

import com.example.agavelab.data.Beer;

import java.util.ArrayList;

public class BeersPresenter  implements BeersInterface.Presenter{

    private BeersInterface.View mainView;
    private BeersInterface.Interactor getInteractor;

    public BeersPresenter(BeersInterface.Interactor getNoticeIntractor) {
        //   this.mainView = mainView;
        this.getInteractor = getNoticeIntractor;
    }


    @Override
    public void setView(BeersInterface.View view) {
        this.mainView = view;
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void requestBeers(Context context, int page, int per_page) {
        if (mainView != null) {
            mainView.hideErrorView();
        }

        getInteractor.getBeers(context, page, per_page, new BeersInterface.Interactor.OnFinishedListener() {
            @Override
            public void onFinished(ArrayList<Beer> beers) {
                if(page==1){
                    mainView.showFirstBeers(beers);
                }else{
                    mainView.showNextBeers(beers);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if(page==1){
                    mainView.onResponseFailure(t);
                }else{
                    mainView.onResponseFailureNext(t);
                }
            }
        });
    }
}
