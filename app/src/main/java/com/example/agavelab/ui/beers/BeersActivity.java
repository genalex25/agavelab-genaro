package com.example.agavelab.ui.beers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.agavelab.R;
import com.example.agavelab.adapters.BeersAdapter;
import com.example.agavelab.adapters.PaginationAdapterCallback;
import com.example.agavelab.adapters.PaginationScrollListener;
import com.example.agavelab.data.Beer;
import com.example.agavelab.di.MyApplication;
import com.example.agavelab.ui.detail.DetailActivity;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BeersActivity extends AppCompatActivity implements PaginationAdapterCallback,BeersInterface.View {

    @Inject BeersPresenter presenter;
    BeersAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    @BindView(R.id.recycler_view)
    RecyclerView rv;
    @BindView(R.id.main_progress)
    ProgressBar progressBar;
    @BindView(R.id.error_layout)
    LinearLayout errorLayout;
    @BindView(R.id.cardView)
    ConstraintLayout btnRetry;
    @BindView(R.id.error_txt_cause)
    TextView txtError;
    @BindView(R.id.title)
    TextView textTitle;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    List<Beer> beers;
    int per_page = 20;
    int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beers);

        ButterKnife.bind(this);

        ((MyApplication) getApplication()).component().inject(this);

        //Setting view to presenter
        presenter.setView(this);

        adapter = new BeersAdapter(BeersActivity.this, this, (View v) -> {
            int position;
            Object tag = v.getTag();
            if (tag != null) {
                position = (Integer) tag;
                System.out.println("position : " + position);
                Intent intent = new Intent(BeersActivity.this, DetailActivity.class);
                intent.putExtra("beer",beers.get(position));
                startActivity(intent);
            }

        });

        beers = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(BeersActivity.this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapter);
        //Setting scroll listener for the
        //"count to reload" is the items to reload before I get to the bottom of the list
        rv.addOnScrollListener(new PaginationScrollListener(linearLayoutManager, 10) {
            @Override
            protected void loadMoreItems() {
                if (page!=0) {
                    isLoading = true;
                    page++;
                    presenter.requestBeers(BeersActivity.this,page,per_page);
                }
            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });

        presenter.requestBeers(this,page, per_page);

    }

    @OnClick(R.id.cardView)
    public void retry(){
        isLoading = false;
        isLastPage = false;
        presenter.requestBeers(this,page, per_page);
    }

    @Override
    public void onResponseFailure(Throwable t) {
        t.printStackTrace();
        showErrorView(t);
    }

    @Override
    public void onResponseFailureNext(Throwable t) {
        t.printStackTrace();
        adapter.showRetry(true, fetchErrorMessage(t));
    }

    @Override
    public void showFirstBeers(ArrayList<Beer> beer) {
        hideErrorView();
        beers.clear();
        adapter.removeAll();
        progressBar.setVisibility(View.GONE);

            if (beer.size() <= 0) {
                if (errorLayout.getVisibility() == View.GONE) {
                    errorLayout.setVisibility(View.VISIBLE);
                    textTitle.setText(getString(R.string.action_no_user));
                    txtError.setText("");
                    btnRetry.setVisibility(View.GONE);
                }
            }

            adapter.addAll(beer);
            beers.addAll(beer);

            if (page>17) {
                //page = 0;
                isLastPage = true;
            } else {
                page++;
                adapter.addLoadingFooter();
            }

    }

    @Override
    public void showNextBeers(ArrayList<Beer> beer) {
        adapter.removeLoadingFooter();
        isLoading = false;

            adapter.addAll(beer);
            beers.addAll(beer);

             if (page>17) {
               //page = 0;
               isLastPage = true;
             } else {
               page++;
               adapter.addLoadingFooter();
             }


    }

    @Override
    public void showErrorView(Throwable throwable) {
        beers.clear();
        adapter.removeAll();
        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            txtError.setText(fetchErrorMessage(throwable));

        }
    }

    @Override
    public void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            textTitle.setText(getString(R.string.action_no_user));
            txtError.setText(getString(R.string.error_response));
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public String fetchErrorMessage(Throwable throwable) {
        return getResources().getString(R.string.error_msg_unknown);
    }

    @Override
    public void retryPageLoad() {
        if (page!=0) {
            presenter.requestBeers(BeersActivity.this, page,per_page);
        }
    }


}
