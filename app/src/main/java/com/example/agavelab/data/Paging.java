package com.example.agavelab.data;


/**
 * Paging Community.
 *
 * This model is used to manage the paging of the lists .
 *
 * Created on October 24, 2018
 *
 * @author gnuno@cartodata.com
 * @version 2018.1
 * @since 1.0
 *
 */
public class Paging {

    private long count;
    private String previous_cursor;
    private String next_cursor;


    public Paging(){

    }

    public Paging(long count, String previous_cursor, String next_cursor) {
        this.count = count;
        this.previous_cursor = previous_cursor;
        this.next_cursor = next_cursor;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getPrevious_cursor() {
        return previous_cursor;
    }

    public void setPrevious_cursor(String previous_cursor) {
        this.previous_cursor = previous_cursor;
    }

    public String getNext_cursor() {
        return next_cursor;
    }

    public void setNext_cursor(String next_cursor) {
        this.next_cursor = next_cursor;
    }
}


