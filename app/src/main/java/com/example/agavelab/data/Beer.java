package com.example.agavelab.data;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Beer.
 *
 * This model is used to manage the beers .
 *
 * Created on November 29, 2018
 *
 * @author gnuno@cartodata.com
 * @version 2018.1
 * @since 1.0
 *
 */
public class Beer implements Serializable {

    private long id;
    private String name;
    private String tagline;
    private String first_brewed;
    private String description;
    private String image_url;
    private float abv;
    private float ibu;
    private float srm;
    private ArrayList<String> food_pairing;

    public Beer(){
    }

    public ArrayList<String> getFood_pairing() {
        return food_pairing;
    }

    public void setFood_pairing(ArrayList<String> food_pairing) {
        this.food_pairing = food_pairing;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getFirst_brewed() {
        return first_brewed;
    }

    public void setFirst_brewed(String first_brewed) {
        this.first_brewed = first_brewed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public float getAbv() {
        return abv;
    }

    public void setAbv(float abv) {
        this.abv = abv;
    }

    public float getIbu() {
        return ibu;
    }

    public void setIbu(float ibu) {
        this.ibu = ibu;
    }

    public float getSrm() {
        return srm;
    }

    public void setSrm(float srm) {
        this.srm = srm;
    }
}
