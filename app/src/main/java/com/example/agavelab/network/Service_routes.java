package com.example.agavelab.network;


import com.example.agavelab.data.Beer;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Service_routes {


    /**
     * -----------------------------------------------------------------
     * ---------------------Routes for Login-------------------------
     */

    @GET("beers")
    Observable<Response<ArrayList<Beer>>> getBeers(@Query("page") int page,
                                                   @Query("per_page") int per_page);
}
