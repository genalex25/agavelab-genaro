package com.example.agavelab.network;


import com.google.gson.GsonBuilder;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Service
 * <p>
 * Service to get the instance retrofit
 * <p>
 * Created on October 27, 2018
 *
 * @author gnuno@cartodata.com
 * @version 2018.1
 * @since 1.0
 */

public class Service {

    private static final String API_BASE_URL_API     = "https://api.punkapi.com/v2/";

    /**
     * This method create the service in mode normal (login)
     */
    public static <S> S createService(Class<S> serviceClass) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(API_BASE_URL_API)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().serializeNulls().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());


        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }


}
