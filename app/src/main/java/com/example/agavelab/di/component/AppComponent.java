package com.example.agavelab.di.component;

import com.example.agavelab.di.MyApplication;
import com.example.agavelab.di.module.AppModule;
import com.example.agavelab.di.module.BeersModule;
import com.example.agavelab.ui.beers.BeersActivity;

import javax.inject.Singleton;
import dagger.Component;

/**
 * App Component
 *
 * Declare all the modules to inject
 *
 * Created on November 24, 2018
 *
 * @author gnuno@cartodata.com
 * @version 2018.1
 * @since 1.0
 *
 */

@Singleton
@Component(modules = {
        AppModule.class,
        BeersModule.class
})
public interface AppComponent {
  void inject(MyApplication categoryApplication);
  void inject(BeersActivity beersActivity);

}
