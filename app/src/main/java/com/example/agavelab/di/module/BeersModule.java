package com.example.agavelab.di.module;

import com.example.agavelab.ui.beers.BeersInteractor;
import com.example.agavelab.ui.beers.BeersPresenter;
import dagger.Module;
import dagger.Provides;

@Module
public class BeersModule {

    @Provides
    public BeersPresenter provideCommunitiesPresenter(BeersInteractor interactor) {
        return new BeersPresenter(interactor);
    }

   // @Singleton
    @Provides
    public BeersInteractor provideCommunitiesInteractor(){
        return new BeersInteractor();
    }



}
