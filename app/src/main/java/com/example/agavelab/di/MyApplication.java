package com.example.agavelab.di;


import android.app.Application;
import android.content.Context;
import androidx.appcompat.app.AppCompatDelegate;
import com.example.agavelab.di.component.AppComponent;
import com.example.agavelab.di.component.DaggerAppComponent;
import com.example.agavelab.di.module.AppModule;
import com.example.agavelab.di.module.BeersModule;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

/**
 * My application.
 *
 * Set the initial settings.
 *
 * Created on October 24, 2018
 *
 *
 * @author gnuno@cartodata.com
 * @version 2018.1
 * @since 1.0
 *
 */
public class MyApplication extends Application {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private AppComponent component;

    public static MyApplication get(Context context) {
        return (MyApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();


        //init dagger2
        setupGraph();

        // Init logger
        Logger.addLogAdapter(new AndroidLogAdapter());

    }

    public void setupGraph() {
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .beersModule(new BeersModule())
                .build();
    }

    public AppComponent component() {
        return component;
    }


}
