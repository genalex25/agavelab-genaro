package com.example.agavelab.di.module;

import android.app.Application;

import com.example.agavelab.di.MyApplication;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

@Module public class AppModule {

  private MyApplication application;

  public AppModule(MyApplication categoryApplication) {
    this.application = categoryApplication;
  }

  @Provides
  @Singleton
  public Application provideApplication() {
    return application;
  }
}
