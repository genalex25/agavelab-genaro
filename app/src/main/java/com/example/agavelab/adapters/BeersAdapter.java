package com.example.agavelab.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.agavelab.R;
import com.example.agavelab.data.Beer;
import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Communities Single Adapter
 * <p>
 * Adapter to show the communities on communities fragment
 * <p>
 * Created on October 24, 2018
 *
 * @author gnuno@cartodata.com
 * @version 2018.1
 * @since 1.0
 */
public class BeersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private List<Beer> beers;
    private Context context;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private PaginationAdapterCallback mCallback;
    private String errorMsg;
    private View.OnClickListener mListener;


    public BeersAdapter(Context context, PaginationAdapterCallback callback, View.OnClickListener listener) {
        this.context = context;
        this.mCallback = callback;
        beers = new ArrayList<>();
        this.mListener = listener;
    }

    /**
     * Creating holder depends of the view (Loading footer or Community item)
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.item_beer, parent, false);
                viewHolder = new CommunitieVH(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    /**
     * Bind view holder depends of the view (Loading footer or Community item)
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Beer beer = beers.get(position);

        switch (getItemViewType(position)) {

            case ITEM:
                final CommunitieVH holds = (CommunitieVH) holder;
                holds.name.setText(beer.getName());
                holds.txtTagline.setText(beer.getTagline());

                Glide.with(context)
                        .load(beer.getImage_url())
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_beer)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                        .into(holds.image);

                /*if (community.getColor() != null) {
                    String newString = community.getColor();
                    int myColor = Color.parseColor(newString);
                    holds.image.setBorderColor(myColor);
                    // holds.imgHouse.setColorFilter(myColor, PorterDuff.Mode.SRC_IN);
                } else {
                    holds.image.setBorderColor(Color.GRAY);
                }*/


                holder.itemView.setOnClickListener(mListener);
                holder.itemView.setTag(position);

                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return beers == null ? 0 : beers.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == beers.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /**
     * Adding community
     */
    public void add(Beer r) {
        beers.add(r);
        notifyItemInserted(beers.size() - 1);
    }

    /**
     * Adding all the communities
     */
    public void addAll(List<Beer> moveResults) {
        isLoadingAdded = false;
        for (Beer result : moveResults) {
            add(result);
        }
    }

    public void removeAll() {
        beers.clear();
        notifyDataSetChanged();
    }


    /**
     * Adding all the loading
     */
    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Beer());
    }

    /**
     * Removing all the loading
     */
    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = beers.size() - 1;
        Beer result = getItem(position);

        if (result != null) {
            beers.remove(position);
            notifyItemRemoved(position);
        }
    }

    private Beer getItem(int position) {
        return beers.get(position);
    }

    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param show     to
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(beers.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    /**
     * Main list's content ViewHolder
     */
    private class CommunitieVH extends RecyclerView.ViewHolder {
        private CircleImageView image;
        private TextView name,txtTagline;

        private CommunitieVH(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.icon);
            name = itemView.findViewById(R.id.txtName);
            txtTagline = itemView.findViewById(R.id.txtTagline);

        }

    }

    /**
     * Loading content ViewHolder
     */
    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        private LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

}
