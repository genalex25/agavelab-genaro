package com.example.agavelab.adapters;

public interface PaginationAdapterCallback {

    void retryPageLoad();
}
